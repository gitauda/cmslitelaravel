<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CoreCrud extends Model
{
    /*
     *
     * To load libraries/Model/Helpers/Add custom code which will be used in this Model
     * This can ease the loading work
     *
     */
    public function __construct(){

        parent::__construct();

        //libraries

        //Helpers

        //Models
        $coreForm = new CoreForm;

        // Your own constructor code
    }




    /*
 *
 * This function help you to select and retun specific column value
 * You can only select single column value
 *
 * In this function you pass
 *
 * 1: Module name / Table name
 *  -> This will be singularize and used to generate column Name
 *  -> Also pluralize for Table Name
 *
 * 2: Pass the selected column name
 * 3: Pass the comparison values
 *  array('column'=>'value')
 *
 * 4: Pass clause if you want to use Like etc.
 *
 * NB: Full Column Name -- will be added by the function
 *
 */
    public function selectSingleValue($module,$select,$where,$clause=null)
    {

        //Modules
        $CoreForm =  new CoreForm;

        $module = $CoreForm->singularize($module);
        $table = $CoreForm->pluralize($module);

        //Columns
        $select_column = $CoreForm->get_column_name($module,$select);
        foreach ($where as $key => $value) {

            $column = $CoreForm->get_column_name($module,$key);
            $where_column[$column] = $value; //Set Proper Column Name
        }

//        get Laravel style Columns
//        $where_column = $CoreForm->getWhereClause($where_column);



        //Check If Clause specified
        if (!is_null($clause)) {

            $selectData = $this->db->select($select_column)->$clause($where_column)->limit(1)->get($table);
            $checkData = $this->checkResultFound($selectData); //Check If Value Found
            $value = ($checkData == true)? $selectData->row()->$select_column : null;

        }else{

            $value = DB::table($table)->where($where_column)->limit(1)->value($select_column);

        }

        //Return Data
        return $value;
    }


}
